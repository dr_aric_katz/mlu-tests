package com.amplio.mlutestapp.logic.speech;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.amplio.mlutestapp.globals.GlobalData;

import java.util.ArrayList;
import java.util.Date;

public class SpeechRecognitionListener implements RecognitionListener
{
    private  Handler mHandler;
    private final String Tag = "SpeechRecog";
    private volatile static Date ts = new Date();
    private volatile static long diffInMs =0;
    public interface OnStopListeningEventListener{
        public void stoppedListening(String txt);
        public void writeToScreen(String txt);
    }
    private static OnStopListeningEventListener mListener;
    public static void setCharacteristicReadEventListener(OnStopListeningEventListener eventListener) {
        mListener=eventListener;
    }


    @Override
    public void onBeginningOfSpeech()
    {
        mHandler = new Handler(Looper.getMainLooper());
        Log.d("tag", "onBeginingOfSpeech");

    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {

    }

    @Override
    public void onEndOfSpeech()
    {
        Log.d("tag", "onEndOfSpeech"); //$NON-NLS-1$

    }

    @Override
    public void onError(int error)
    {
        /**
         * Error 1 ERROR_NETWORK_TIMEOUT
         * Error 2 ERROR_NETWORK
         * Error 3 ERROR_AUDIO
         * Errro 4 ERROR_SERVER
         * Error 5 ERROR_CLIENT
         * Error 6 ERROR_SPEECH_TIMEOUT
         * Error 7 ERROR_NO_MATCH
         * Error 8 ERROR_RECOGNIZER_BUSY
         * Error 9 ERROR_INSUFFICIENT_PERMISSIONS
         */
        if(error==7 && !GlobalData.stopedRecording){
            mListener.stoppedListening( "");
        }
        Log.d(Tag, "error = " + error); //$NON-NLS-1$
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {

    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {
        Date currentTime = new Date();

        diffInMs = currentTime.getTime() - ts.getTime();
        ts = currentTime;

        //print to screen
        ArrayList data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String word = (String) data.get(data.size() - 1);
        if(word.startsWith(" "))
            word = word.substring(1,word.length()-1);

        Log.d(Tag, "word = " + word);
        mListener.writeToScreen(word + " ");

    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {
        Log.d("tag", "onReadyForSpeech"); //$NON-NLS-1$
    }

    @Override
    public void onResults(Bundle results)
    {
        if((results!=null) && results.containsKey(SpeechRecognizer.RESULTS_RECOGNITION)){

            ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            String speechData = data.get(0);

            Log.d(Tag, "speech = " + speechData );
            // added to get the last word
            mListener.writeToScreen(speechData + " ");
//            if(diffInMs>GlobalData.threshold)
//                speechData +="\r\n";
//            else
//                speechData +=" ";

            final String txt = speechData;
            // use callback to start again
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mListener.stoppedListening(speechData + "\r\n");
                }
            }, 10);

        }
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {

    }

}
