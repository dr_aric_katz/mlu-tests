package com.amplio.mlutestapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.SpeechRecognizer;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.widget.TextView;

import com.amplio.mlutestapp.globals.GlobalData;
import com.amplio.mlutestapp.logic.speech.SpeechRecognitionListener;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;


public class MainActivity extends AppCompatActivity {
    private static int RecRequestCode = 4;
    private boolean audioRecordingPermissionGranted = false;
    private boolean isRecording =false;
    private ImageView mic;
    private TextView mainTxt;
    private TextView utterancesTxt;
    private TextView morphemesTxt;
    private TextView mluTxt;
    private TextView ageFitTxt;
    private EditText threshold;
    private final SpeechRecognizer speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
    private Intent speechRecognizerIntent;
    private Handler mainHandler = new Handler(Looper.getMainLooper());
    private static volatile String baseTxt ="";
    private final String Tag = "Main";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkPermissionsRecording();
        setupUI();
        setupSpeachRecognizer();
        signupCallbackEvents();

    }

    private void setupUI(){
        setContentView(R.layout.activity_main);

        mic =  findViewById(R.id.mic);
        mainTxt = findViewById(R.id.main_txt);
        mainTxt.setElegantTextHeight(true);
        mainTxt.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        mainTxt.setSingleLine(false);
        threshold = findViewById(R.id.threshold);
        utterancesTxt = findViewById(R.id.utterances_txt);
        morphemesTxt = findViewById(R.id.morpheme_txt);
        mluTxt = findViewById(R.id.mlu_txt);
        ageFitTxt = findViewById(R.id.age_fit_txt);

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRecording){
                    isRecording = false;
                    mic.setImageTintList( ColorStateList.valueOf(getColor(R.color.wear_yellow)));
                    speechRecognizer.stopListening();
                    stopMicAnimation(mic);
                    GlobalData.stopedRecording = true;
                    String fs = mainTxt.getText().toString();
                    if(!fs.trim().equals(""))
                        printMLUResults(fs);
                }
                else {
                    GlobalData.stopedRecording = false;
                    clearUI();
                    startAudioRecording();
                }
            }
        });
    }

    private void clearUI(){
        mainTxt.setText("");
        baseTxt = "";
        utterancesTxt.setText("0");
        morphemesTxt.setText("0");
        mluTxt.setText("0");
        ageFitTxt.setText("0");
    }

    private void printMLUResults(String finalTxt){
        try {
            String[] utterances = finalTxt.split("\n");
            int utterancesCount = utterances.length;

            int morphemeCount = 0;
            for (String u : utterances) {
                String[] words = u.split(" ");
                for (String word : words) {
                    morphemeCount++;
                    if (GlobalData.DoubleStrings.contains(word.toLowerCase()))
                        morphemeCount++;
                    // apply double count to double weighted words
                    for (String doubler : GlobalData.DoubleChars) {
                        if (word.length()>2 && word.endsWith(doubler) &&
                                !word.toLowerCase().equals("this")) {
                            morphemeCount++;
                            
                            if (word.endsWith("ed") && word.length()<=3)
                                morphemeCount--;
                        }
                    }

                    // reduce morpheme if it is no count word
                    for (String nc : GlobalData.NoCounts) {
                        if (word.toLowerCase().equals(nc))
                            morphemeCount--;
                    }
                }
            }

            double mlu = (morphemeCount * 1.0) / (utterancesCount * 1.0);
            int starter = 0;
            int ended = 0;
            int preVal = 0;

            // use binary search of O(log2N) *data is ordered.
            // instead of linear search O(N)
            Iterator<Map.Entry<Double, Integer>> it = GlobalData.MLUByAge.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Double, Integer> mluIn = it.next();
                //for(Map.Entry<Float, Integer> mluIn: GlobalData.MLUByAge.entrySet()){
                Double key = mluIn.getKey();

                if (key == mlu) {
                    starter = mluIn.getValue();
                    break;
                } else if (key > mlu) {
                    ended = mluIn.getValue();
                    starter = preVal;
                    break;
                }
                preVal = mluIn.getValue();
            }


            utterancesTxt.setText(String.valueOf(utterancesCount));
            morphemesTxt.setText(String.valueOf(morphemeCount));
            String mluAdg = String.valueOf(mlu).length() > 4 ? String.valueOf(mlu).substring(0, 4) : String.valueOf(mlu);
            mluTxt.setText(mluAdg);
            if (starter == 0 && ended == 0)
                ageFitTxt.setText("Greater than 60 months");
            else if (starter > 0 && ended == 0)
                ageFitTxt.setText(String.valueOf(starter) + " months");
            else
                ageFitTxt.setText("between " + String.valueOf(starter) + " and " + String.valueOf(ended) + " months");
        }
        catch (Exception ex){
            Log.d(Tag, ex.getMessage());
        }
    }

    private void signupCallbackEvents(){
        SpeechRecognitionListener.setCharacteristicReadEventListener(new SpeechRecognitionListener.OnStopListeningEventListener() {
            @Override
            public void stoppedListening(String txt) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        baseTxt += txt;
                        speechRecognizer.stopListening();
                        startAudioRecording();
                    }
                });
            }

            @Override
            public void writeToScreen(String txt) {
                //String inTxt = mainTxt.getText().toString();
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mainTxt.setText(baseTxt + txt );
                    }
                });
            }
        });
    }

    private void startAudioRecording(){

        GlobalData.threshold =Float.valueOf(threshold.getText().toString());
        isRecording = true;
        mic.setImageTintList( ColorStateList.valueOf(Color.RED));
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        speechRecognizer.startListening(speechRecognizerIntent);
        setMicAnimation(mic);
    }

    private void setMicAnimation(ImageView image){
        Animation animation = new AlphaAnimation((float) 0.5, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the
        image.startAnimation(animation);
    }

    private void stopMicAnimation(ImageView image){
        image.clearAnimation();

    }

    private void setupSpeachRecognizer(){
        speechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        String languagePref = "en-US";
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, languagePref);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, languagePref);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, languagePref);
    }


    private void checkPermissionsRecording() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, //skip this permission if your app does not target Android 10 or above
                            Manifest.permission.RECORD_AUDIO
                    },
                    RecRequestCode);
    }

}