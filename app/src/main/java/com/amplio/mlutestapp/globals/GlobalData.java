package com.amplio.mlutestapp.globals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GlobalData {
    public static float threshold =1000f;
    public static boolean stopedRecording = false;

    public static final Map<Double, Integer> MLUByAge = new TreeMap(){{
        put(1.31, 18);
        put(1.62, 21);
        put(1.92, 24);
        put(2.54, 30);
        put(2.85, 33);
        put(3.16, 36);
        put(3.47, 39);
        put(3.78, 42);
        put(4.09, 45);
        put(4.4, 48);
        put(4.71, 51);
        put(5.02, 54);
        put(5.32, 57);
        put(5.63, 60);
    }};

    public static final List<String> DoubleChars = Arrays.asList("s", "ed");

    public static final List<String> DoubleStrings = Arrays.asList("she’s",
            "he’ll", "they’re", "what’s", "she’d", "we’ve", "can’t", "aren’t");

    public static final List<String> NoCounts = Arrays.asList("um", "well", "oh", "um", "hmm");
}
