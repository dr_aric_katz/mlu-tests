package com.amplio.mlutestapp.ui.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.amplio.mlutestapp.MainActivity;


import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import com.amplio.mlutestapp.R;

public class SplashActivity extends AppCompatActivity {
    private static Animation animation;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
        catch (NullPointerException e){}
        setContentView(R.layout.activity_splash);
        ImageView iv=(ImageView) findViewById(R.id.cLogo);
        animation= AnimationUtils.loadAnimation(SplashActivity.this,R.anim.blink_anim);


        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.MATCH_CONSTRAINT);

        params.bottomToBottom = ConstraintSet.PARENT_ID;
        params.topToTop= ConstraintSet.PARENT_ID;
        params.startToStart = ConstraintSet.PARENT_ID;
        params.endToEnd = ConstraintSet.PARENT_ID;
        float density = SplashActivity.this.getResources().getDisplayMetrics().density;
        float px = (75 * density)*2;
        params.width = (int)px;
        iv.setLayoutParams(params);
        iv.startAnimation(animation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                iv.clearAnimation();
                float px = (150* density)*2;
                params.width = (int)px;
                iv.setLayoutParams(params);
                animation.cancel();
                animation.reset();
            }
        }, 2500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);


                startActivity(intent);
                SplashActivity.this.finish();

            }
        }, 2500);

    }
}
